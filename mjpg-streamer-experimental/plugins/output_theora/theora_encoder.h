/*******************************************************************************
#                                                                              #
#      MJPG-streamer allows to stream JPG frames from an input-plugin          #
#      to several output plugins                                               #
#                                                                              #
#      Copyright (C) 2007 Tom Stöveken                                         #
#                                                                              #
# This program is free software; you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation; version 2 of the License.                      #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program; if not, write to the Free Software                  #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    #
#                                                                              #
*******************************************************************************/

#ifndef THEORA_ENCODER_H
#define THEORA_ENCODER_H

typedef struct _buffer_pool buffer_pool;

struct _buffer_pool {
    int pic_width;              /* picture width for Theora encoding */
    int pic_height;             /* picture height for Theora encoding */
    int num_buffers;            /* total number of buffers in the pool */
    uint32_t status;            /* each bit indicate the status of one buffer, up to 32 */
    uint8_t **buffer_ptr;       /* array of pointers to every buffer */
    uint8_t *raw;               /* the big chunk of buffer pool */
    pthread_mutex_t m;
    pthread_cond_t update;
};

typedef struct _theora_context theora_context;

struct _theora_context {
    int width;                  /* picture width */
    int height;                 /* picture height */
    float fps;                  /* frames per second */
    int bitrate;                /* output bit rate in kbps */
    globals *pglobal;           /* global information */
    FILE *out;                  /* the output file */
    uint64_t stream_size;       /* current encoded stream size */
    uint64_t num_frames;        /* current number of encoded frames */
    buffer_pool bp;             /* buffer pool between the grabber and the encoder */
};

void *encoder_thread(void *arg);

#endif /* THEORA_ENCODER_H */
