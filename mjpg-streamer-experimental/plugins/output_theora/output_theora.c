/*******************************************************************************
#                                                                              #
#      MJPG-streamer allows to stream JPG frames from an input-plugin          #
#      to several output plugins                                               #
#                                                                              #
#      Copyright (C) 2007 Tom Stöveken                                         #
#                                                                              #
# This program is free software; you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation; version 2 of the License.                      #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program; if not, write to the Free Software                  #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    #
#                                                                              #
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <getopt.h>
#include <pthread.h>
#include <fcntl.h>
#include <time.h>
#include <syslog.h>

#include "../../utils.h"
#include "../../mjpg_streamer.h"
#include "theora_encoder.h"

#define OUTPUT_PLUGIN_NAME "Theora output plugin"

static theora_context tc;
static pthread_t grabber;
static pthread_t encoder;
static int delay;
static uint8_t *frame = NULL;
static char *file_path = NULL;
static char *command = NULL;
static int input_number = 0;

static int pop32(uint32_t x) {
    x = x - ((x >> 1) & 0x55555555);
    x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
    x = (x + (x >> 4)) & 0x0F0F0F0F;
    x = x + (x >> 8);
    x = x + (x >> 16);
    return x & 0x0000003F;
}

/******************************************************************************
Description.: convert from YUYV 4:2:2 to I420 4:2:0
Input Value.: buffer pointers, size information
Return Value:
******************************************************************************/
static void convert_yuyv_to_i420(uint8_t *dst, uint8_t *src, int width, int height)
{
    int i, j;
    uint8_t *src_line0, *src_line1;
    uint8_t *dst_line_y, *dst_line_cb, *dst_line_cr;
    uint32_t src0[4];
    uint32_t src1[4];

    /*
     * we don't bother to check the buffer pointers here, and assume the width
     * will be multiple of 4, height will be multiple of 2.
     */
    src_line0 = src;
    src_line1 = src_line0 + (width << 1);
    dst_line_y = dst;
    dst_line_cb = dst_line_y + width * height;
    dst_line_cr = dst_line_cb + (width * height >> 2);

    for(i = 0; i < height; i += 2) {
        for(j = 0; j < (width << 1); j += 16) {
            uint32_t tmp0, tmp1;
            uint32_t packed0, packed1;

            src0[0] = *((uint32_t *)&src_line0[j]);
            src0[1] = *((uint32_t *)&src_line0[j + 4]);
            src0[2] = *((uint32_t *)&src_line0[j + 8]);
            src0[3] = *((uint32_t *)&src_line0[j + 12]);

            src1[0] = *((uint32_t *)&src_line1[j]);
            src1[1] = *((uint32_t *)&src_line1[j + 4]);
            src1[2] = *((uint32_t *)&src_line1[j + 8]);
            src1[3] = *((uint32_t *)&src_line1[j + 12]);

            /* pack the Y data */
            packed0 = (((src0[0] & 0x00FF00FF) + ((src0[0] & 0x00FF00FF) >> 8)) & 0x0000FFFF) +
                       ((((src0[1] & 0x00FF00FF) + ((src0[1] & 0x00FF00FF) >> 8)) & 0x0000FFFF) << 16);
            *((uint32_t *)&dst_line_y[j >> 1]) = packed0;
            packed1 = (((src0[2] & 0x00FF00FF) + ((src0[2] & 0x00FF00FF) >> 8)) & 0x0000FFFF) +
                       ((((src0[3] & 0x00FF00FF) + ((src0[3] & 0x00FF00FF) >> 8)) & 0x0000FFFF) << 16);
            *((uint32_t *)&dst_line_y[(j >> 1) + 4]) = packed1;
            packed0 = (((src1[0] & 0x00FF00FF) + ((src1[0] & 0x00FF00FF) >> 8)) & 0x0000FFFF) +
                       ((((src1[1] & 0x00FF00FF) + ((src1[1] & 0x00FF00FF) >> 8)) & 0x0000FFFF) << 16);
            *((uint32_t *)&dst_line_y[(j >> 1) + width]) = packed0;
            packed1 = (((src1[2] & 0x00FF00FF) + ((src1[2] & 0x00FF00FF) >> 8)) & 0x0000FFFF) +
                       ((((src1[3] & 0x00FF00FF) + ((src1[3] & 0x00FF00FF) >> 8)) & 0x0000FFFF) << 16);
            *((uint32_t *)&dst_line_y[(j >> 1) + 4 + width]) = packed1;

            /* pack the Cb and Cr data */
            tmp0 = ((src0[0] & 0xFF00FF00) >> 8) + ((src1[0] & 0xFF00FF00) >> 8);
            tmp0 = ((tmp0 + 0x00010001) >> 1) & 0x00FF00FF;
            tmp1 = ((src0[1] & 0xFF00FF00) >> 8) + ((src1[1] & 0xFF00FF00) >> 8);
            tmp1 = ((tmp1 + 0x00010001) >> 1) & 0x00FF00FF;
            packed0 = ((tmp0 + (tmp0 >> 8)) & 0x0000FFFF) + (((tmp1 + (tmp1 >> 8)) & 0x0000FFFF) << 16);
            tmp0 = ((src0[2] & 0xFF00FF00) >> 8) + ((src1[2] & 0xFF00FF00) >> 8);
            tmp0 = ((tmp0 + 0x00010001) >> 1) & 0x00FF00FF;
            tmp1 = ((src0[3] & 0xFF00FF00) >> 8) + ((src1[3] & 0xFF00FF00) >> 8);
            tmp1 = ((tmp1 + 0x00010001) >> 1) & 0x00FF00FF;
            packed1 = ((tmp0 + (tmp0 >> 8)) & 0x0000FFFF) + (((tmp1 + (tmp1 >> 8)) & 0x0000FFFF) << 16);
            tmp0 = (((packed0 & 0x00FF00FF) + ((packed0 & 0x00FF00FF) >> 8)) & 0x0000FFFF) +
                    ((((packed1 & 0x00FF00FF) << 16) + ((packed1 & 0x00FF00FF) << 8)) & 0xFFFF0000);
            *((uint32_t *)&dst_line_cb[j >> 2]) = tmp0;
            tmp1 = ((((packed0 & 0xFF00FF00) >> 16) + ((packed0 & 0xFF00FF00) >> 8)) & 0x0000FFFF) +
                    (((packed1 & 0xFF00FF00) + ((packed1 & 0xFF00FF00) << 8)) & 0xFFFF0000);
            *((uint32_t *)&dst_line_cr[j >> 2]) = tmp1;
        }
        src_line0 += width << 2;
        src_line1 = src_line0 + (width << 1);
        dst_line_y += width << 1;
        dst_line_cb += width >> 1;
        dst_line_cr += width >> 1;
    }
}

/******************************************************************************
Description.: print a help message
Input Value.: -
Return Value: -
******************************************************************************/
void help(void)
{
    fprintf(stderr, " ---------------------------------------------------------------\n" \
            " Help for output plugin..: "OUTPUT_PLUGIN_NAME"\n" \
            " ---------------------------------------------------------------\n" \
            " The following parameters can be passed to this plugin:\n\n" \
            " [-f | --file ]..........: the path for the output OGV file\n" \
            " [-d | --delay ].........: delay after saving pictures in ms\n" \
            " [-s | --size ]..........: size of ring buffer (max number of pictures to hold)\n" \
            " [-c | --command ].......: execute command after saving picture\n" \
            " [-i | --input ].........: read frames from the specified input plugin\n" \
            " [-r | --bitrate ].......: output bitrate in kbps\n" \
            " ---------------------------------------------------------------\n");
}

/******************************************************************************
Description.: clean up allocated resources by the grabber thread
Input Value.: unused argument
Return Value:
******************************************************************************/
void grabber_cleanup(void *arg)
{
    static int first_run = 1;

    if(!first_run) {
        DBG("already cleaned up resources\n");
        return;
    }

    first_run = 0;
    OPRINT("cleaning up resources allocated by grabber thread\n");

    pthread_mutex_lock(&tc.bp.m);
    pthread_cond_signal(&tc.bp.update);
    pthread_mutex_unlock(&tc.bp.m);

    /* wait until the encoder thread terminates cleanly */
    OPRINT("waiting for encoder thread to terminate\n");
    pthread_join(encoder, NULL);

    if(stdout != tc.out) {
        fclose(tc.out);
    }

    if(frame) {
        free(frame);
    }

    if(tc.bp.buffer_ptr) {
        free(tc.bp.buffer_ptr);
    }

    if(tc.bp.raw) {
        free(tc.bp.raw);
    }
    pthread_cond_destroy(&tc.bp.update);
    pthread_mutex_destroy(&tc.bp.m);
}

/******************************************************************************
Description.: this is the main worker thread
              it loops forever, grabs a fresh frame and stores it to buffer pool
Input Value.:
Return Value:
******************************************************************************/
void *grabber_thread(void *arg)
{
    buffer_pool *bufp = &tc.bp;
    uint8_t *frame_ptr;
    int frame_size;

    DBG("grabber thread running\n");

    /* set cleanup handler to cleanup allocated ressources */
    pthread_cleanup_push(grabber_cleanup, NULL);

    while(!tc.pglobal->stop) {
        /* check if we have empty buffer in the buffer pool */
        pthread_mutex_lock(&bufp->m);

        /* keep waiting until not all buffers are filled */
        while(!tc.pglobal->stop && bufp->status == ((1 << bufp->num_buffers) - 1)) {
            pthread_cond_wait(&bufp->update, &bufp->m);
        }
        frame_ptr = bufp->buffer_ptr[pop32(bufp->status)];
        pthread_mutex_unlock(&bufp->m);

        if(tc.pglobal->stop) {
            continue;
        }

        pthread_mutex_lock(&tc.pglobal->in[input_number].db);

        pthread_cond_wait(&tc.pglobal->in[input_number].db_update, &tc.pglobal->in[input_number].db);

        /* read buffer */
        frame_size = tc.pglobal->in[input_number].size;

        /* copy frame to buffer pool */
        memcpy(frame, tc.pglobal->in[input_number].buf, frame_size);

        /* allow others to access the global buffer again */
        pthread_mutex_unlock(&tc.pglobal->in[input_number].db);

        DBG("convert from YUYV @0x%08X to I420 @0x%08X\n", (int)frame, (int)frame_ptr);
        /* convert the captured frame from YUYV 4:2:2 to I420 4:2:0 */
        convert_yuyv_to_i420(frame_ptr, frame, bufp->pic_width, bufp->pic_height);

        /* signal to encoder thread a new frame is ready */
        pthread_mutex_lock(&bufp->m);
        bufp->status |= (1 << pop32(bufp->status));
        DBG("buffer status 0x%08X\n", bufp->status);
        pthread_cond_signal(&bufp->update);
        pthread_mutex_unlock(&bufp->m);

        /* if specified, wait now */
        if(delay > 0) {
            usleep(1000 * delay);
        }
    }

    /* cleanup now */
    pthread_cleanup_pop(1);

    return NULL;
}

/*** plugin interface functions ***/
/******************************************************************************
Description.: this function is called first, in order to initialize
              this plugin and pass a parameter string
Input Value.: parameters
Return Value: 0 if everything is OK, non-zero otherwise
******************************************************************************/
int output_init(output_parameter *param)
{
    int i;
    delay = 0;

    param->argv[0] = OUTPUT_PLUGIN_NAME;
    memset(&tc, 0, sizeof(tc));
    tc.bp.num_buffers = 1;

    /* show all parameters for DBG purposes */
    for(i = 0; i < param->argc; i++) {
        DBG("argv[%d]=%s\n", i, param->argv[i]);
    }

    reset_getopt();
    while(1) {
        int option_index = 0, c = 0;
        static struct option long_options[] = {
            {"h", no_argument, 0, 0
            },
            {"help", no_argument, 0, 0},
            {"f", required_argument, 0, 0},
            {"file", required_argument, 0, 0},
            {"d", required_argument, 0, 0},
            {"delay", required_argument, 0, 0},
            {"s", required_argument, 0, 0},
            {"size", required_argument, 0, 0},
            {"c", required_argument, 0, 0},
            {"command", required_argument, 0, 0},
            {"i", required_argument, 0, 0},
            {"input", required_argument, 0, 0},
            {"r", required_argument, 0, 0},
            {"bitrate", required_argument, 0, 0},
            {0, 0, 0, 0}
        };

        c = getopt_long_only(param->argc, param->argv, "", long_options, &option_index);

        /* no more options to parse */
        if(c == -1) break;

        /* unrecognized option */
        if(c == '?') {
            help();
            return 1;
        }

        switch(option_index) {
            /* h, help */
        case 0:
        case 1:
            DBG("case 0,1\n");
            help();
            return 1;
            break;

            /* f, file */
        case 2:
        case 3:
            DBG("case 2,3\n");
            file_path = malloc(strlen(optarg) + 1);
            strcpy(file_path, optarg);
            break;

            /* d, delay */
        case 4:
        case 5:
            DBG("case 4,5\n");
            delay = atoi(optarg);
            break;

            /* s, size */
        case 6:
        case 7:
            DBG("case 6,7\n");
            tc.bp.num_buffers = atoi(optarg);
            break;

            /* c, command */
        case 8:
        case 9:
            DBG("case 8,9\n");
            command = strdup(optarg);
            break;

        case 10:
        case 11:
            DBG("case 10,11\n");
            input_number = atoi(optarg);
            break;

        case 12:
        case 13:
            DBG("case 12,13\n");
            tc.bitrate = atoi(optarg);
            break;
        }
    }

    tc.pglobal = param->global;
    if(!(input_number < tc.pglobal->incnt)) {
        OPRINT("ERROR: the %d input_plugin number is too much only %d plugins loaded\n", input_number, tc.pglobal->incnt);
        return 1;
    }

    OPRINT("output file.......: %s\n", file_path ? file_path : "STDOUT");
    OPRINT("input plugin......: %d: %s\n", input_number, tc.pglobal->in[input_number].plugin);
    OPRINT("delay after save..: %d\n", delay);
    OPRINT("bufferpool size...: %d\n", tc.bp.num_buffers);
    OPRINT("command...........: %s\n", (command == NULL) ? "disabled" : command);
    return 0;
}

/******************************************************************************
Description.: calling this function stops the worker thread
Input Value.: -
Return Value: always 0
******************************************************************************/
int output_stop(int id)
{
    DBG("will cancel grabber thread\n");
    pthread_cancel(grabber);
    return 0;
}

/******************************************************************************
Description.: calling this function creates and starts the worker thread
Input Value.: -
Return Value: always 0
******************************************************************************/
int output_run(int id)
{
    int i;
    buffer_pool *bufp = &tc.bp;
    input *in = &tc.pglobal->in[input_number];

    /* allocate one frame for temporary buffering */
    if(posix_memalign((void **)&frame, sizeof(uint64_t), in->width * in->height << 1)) {
        OPRINT("could not allocate memory\n");
        exit(EXIT_FAILURE);
    }

#if 0
    /* check pixel format, only YUYV supported so far */
    if(V4L2_PIX_FMT_YUYV != in->in_formats[in->currentFormat].format.pixelformat) {
        OPRINT("only V4L2_PIX_FMT_YUYV format currently supported\n");
        exit(EXIT_FAILURE);
    }
#endif

    /* allocate pixel data buffer and initialize the buffer pool */
    tc.width = in->width;
    tc.height = in->height;
    tc.fps = (float)in->fps;
    bufp->pic_width = ((tc.width + 3) >> 2) << 2;
    bufp->pic_height = ((tc.height + 1) >> 1) << 1;
    if(posix_memalign((void **)&bufp->raw, sizeof(uint64_t), bufp->num_buffers * bufp->pic_width * bufp->pic_height * 3 >> 1)) {
        OPRINT("could not allocate memory\n");
        exit(EXIT_FAILURE);
    }
    bufp->buffer_ptr = malloc(bufp->num_buffers * sizeof(uint8_t *));
    if(NULL == bufp->buffer_ptr) {
        OPRINT("could not allocate memory\n");
        exit(EXIT_FAILURE);
    }
    for(i = 0; i < bufp->num_buffers; i++) {
        bufp->buffer_ptr[i] = bufp->raw + i * (bufp->pic_width * bufp->pic_height * 3 >> 1);
    }
    if(pthread_mutex_init(&bufp->m, NULL)) {
        OPRINT("pthread_mutex_init() failed\n");
        exit(EXIT_FAILURE);
    }
    if(pthread_cond_init(&bufp->update, NULL)) {
        OPRINT("pthread_cond_init() failed\n");
        exit(EXIT_FAILURE);
    }

    if(NULL == file_path) {
        tc.out = stdout;
    } else {
        if(NULL == (tc.out = fopen(file_path, "wb"))) {
            OPRINT("fopen() failed\n");
            exit(EXIT_FAILURE);
        }
    }

    DBG("launching grabber thread\n");
    pthread_create(&grabber, 0, grabber_thread, NULL);
    pthread_detach(grabber);

    DBG("launching Theora encoder thread\n");
    pthread_create(&encoder, 0, encoder_thread, (void *)&tc);
#if 0
    /* we would rather join it later after properly terminating the encoder thread */
    pthread_detach(encoder);
#endif

    return 0;
}

int output_cmd(int plugin, unsigned int control_id, unsigned int group, int value)
{
    DBG("command (%d, value: %d) for group %d triggered for plugin instance #%02d\n", control_id, value, group, plugin);
    return 0;
}
