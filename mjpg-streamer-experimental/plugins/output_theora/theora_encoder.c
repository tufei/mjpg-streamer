/*******************************************************************************
#                                                                              #
#      MJPG-streamer allows to stream JPG frames from an input-plugin          #
#      to several output plugins                                               #
#                                                                              #
#      Copyright (C) 2007 Tom Stöveken                                         #
#                                                                              #
# This program is free software; you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation; version 2 of the License.                      #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program; if not, write to the Free Software                  #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    #
#                                                                              #
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>

#include "../../mjpg_streamer.h"
#include <theora/theoraenc.h>
#include "theora_encoder.h"

static theora_context *tc = NULL;
static th_enc_ctx *h = NULL;
static th_info ti;
static th_comment tm;
static ogg_stream_state to;
static ogg_page og;
static ogg_packet op;

/******************************************************************************
Description.: initialize the H.264 encoder
Input Value.:
Return Value:
******************************************************************************/
static int encoder_init(void)
{
    int speed;

    DBG("encoder initialization\n");

    srand(time(NULL));
    if(ogg_stream_init(&to, rand())) {
        OPRINT("ogg_stream_init() failed\n");
        return -1;
    }

    th_info_init(&ti);

    ti.frame_width = tc->width;
    ti.frame_height = tc->height;
    ti.pic_width = tc->width;
    ti.pic_height = tc->height;
    ti.pic_x = 0;
    ti.pic_y = 0;
    ti.fps_numerator = (ogg_uint32_t)(tc->fps * 1000 + 0.5);
    ti.fps_denominator = 1000;
    ti.aspect_numerator = 1;
    ti.aspect_denominator = 1;
    ti.colorspace = TH_CS_UNSPECIFIED;
    ti.pixel_fmt = TH_PF_420;
    ti.target_bitrate = tc->bitrate * 1000;
    ti.quality = 0;

    if(NULL == (h = th_encode_alloc(&ti))) {
        OPRINT("th_encode_alloc() failed\n");
        return -1;
    }
    th_info_clear(&ti);

    /* make speed encoding for resource constrained platform */
    if(th_encode_ctl(h, TH_ENCCTL_GET_SPLEVEL_MAX, &speed, sizeof(speed))) {
        OPRINT("th_encode_ctl() failed\n");
        return -1;
    }

    if(th_encode_ctl(h, TH_ENCCTL_SET_SPLEVEL, &speed, sizeof(speed))) {
        OPRINT("th_encode_ctl() failed\n");
        return -1;
    }

    th_comment_init(&tm);

    OPRINT("theora picture size %dx%d, speed level %d\n", tc->width, tc->height, speed);
    return 0;
}

/******************************************************************************
Description.: terminate the H.264 encoder
Input Value.:
Return Value:
******************************************************************************/
static int encoder_term(void)
{
    if(h) {
        if(ogg_stream_clear(&to)) {
            OPRINT("ogg_stream_clear() failed\n");
            return -1;
        }
        th_encode_free(h);
        th_comment_clear(&tm);
        DBG("encoder closed\n");
    }

    return 0;
}

/******************************************************************************
Description.: clean up allocated resources by the encoder thread
Input Value.: unused argument
Return Value:
******************************************************************************/
void encoder_cleanup(void *arg)
{
    static int first_run = 1;

    if(!first_run) {
        DBG("already cleaned up resources\n");
        return;
    }

    first_run = 0;
    OPRINT("cleaning up resources allocated by encoder thread\n");

    /*
     * conservative signaling in case the buffer pool is full and the grabber
     * is waititng on the conditional variable
     */
    pthread_mutex_lock(&tc->bp.m);
    tc->bp.status = 0;
    pthread_cond_signal(&tc->bp.update);
    pthread_mutex_unlock(&tc->bp.m);

    encoder_term();
}

/******************************************************************************
Description.: retrieve a raw picture from the buffer pool
Input Value.:
Return Value:
******************************************************************************/
static int encoder_get_raw_picture(th_ycbcr_buffer pic)
{
    /* check if we have filled buffer in the buffer pool */
    pthread_mutex_lock(&tc->bp.m);

    /* keep waiting until at least one buffer is filled */
    while(!tc->pglobal->stop && 0 == tc->bp.status) {
        pthread_cond_wait(&tc->bp.update, &tc->bp.m);
    }

    pic[0].data = tc->bp.buffer_ptr[0];
    pthread_mutex_unlock(&tc->bp.m);

    if(tc->pglobal->stop) {
        return 0;
    }

    tc->num_frames++;
    pic[0].width = tc->width;
    pic[0].height = tc->height;
    pic[0].stride = tc->width;

    pic[1].width = tc->width >> 1;
    pic[1].height = tc->height >> 1;
    pic[1].stride = tc->width >> 1;
    pic[1].data = pic[0].data + tc->width * tc->height;

    pic[2].width = tc->width >> 1;
    pic[2].height = tc->height >> 1;
    pic[2].stride = tc->width >> 1;
    pic[2].data = pic[1].data + (tc->width * tc->height >> 2);

    return 0;
}

/******************************************************************************
Description.: encode one frame and do proper house keeping
Input Value.:
Return Value:
******************************************************************************/
static int encoder_compress_picture(th_ycbcr_buffer pic, int last)
{
    int ret;

    if(th_encode_ycbcr_in(h, pic)) {
        OPRINT("th_encode_ycbcr_in() failed\n");
        return -1;
    }

    for(; ;) {
        ret = th_encode_packetout(h, last, &op);
        if(0 > ret) {
            OPRINT("th_encode_packetout() failed\n");
            return -1;
        }
        if(0 == ret)
            break;
        if(ogg_stream_packetin(&to, &op)) {
            OPRINT("ogg_stream_packetin() failed\n");
            return -1;
        }
    }

    for(; ;) {
        ret = ogg_stream_pageout(&to, &og);
        if(0 > ret) {
            OPRINT("ogg_stream_pageout() failed\n");
            return -1;
        }
        if(0 == ret)
            break;
        fwrite(og.header, sizeof(uint8_t), og.header_len, tc->out);
        fwrite(og.body, sizeof(uint8_t), og.body_len, tc->out);
        tc->stream_size += og.header_len + og.body_len;
    }

    return 0;
}

/******************************************************************************
Description.: this is the main encoder thread
              it loops forever, takes a fresh frame from the buffer pool and
              encodes it
Input Value.: the exchange context
Return Value:
******************************************************************************/
void *encoder_thread(void *arg)
{
    th_ycbcr_buffer pic;
    uint8_t *curr_raw;
    int i;

    DBG("encoder thread running\n");

    tc = (theora_context *)arg;

    if(NULL == tc->out) {
        OPRINT("output file not opened\n");
        exit(EXIT_FAILURE);
    }

    /* set cleanup handler to cleanup allocated ressources */
    pthread_cleanup_push(encoder_cleanup, NULL);

    if(encoder_init()) {
        OPRINT("encoder_init() failed\n");
        exit(EXIT_FAILURE);
    }

    /* handle the stream head */
    if(0 >= th_encode_flushheader(h, &tm, &op)) {
        OPRINT("th_encode_flushheader() failed\n");
        exit(EXIT_FAILURE);
    }
    if(ogg_stream_packetin(&to, &op)) {
        OPRINT("ogg_stream_packetin() failed\n");
        exit(EXIT_FAILURE);
    }
    /* first packet gets its own page automatically */
    if(1 != ogg_stream_pageout(&to, &og)) {
        OPRINT("ogg_stream_pageout() failed\n");
        exit(EXIT_FAILURE);
    }
    fwrite(og.header, sizeof(uint8_t), og.header_len, tc->out);
    fwrite(og.body, sizeof(uint8_t), og.body_len, tc->out);

    /* create the remaining theora headers */
    for(; ;) {
        int ret;

        ret = th_encode_flushheader(h, &tm, &op);
        if(0 > ret) {
            OPRINT("th_encode_flushheader() failed\n");
            exit(EXIT_FAILURE);
        }
        if(0 == ret)
            break;
        if(ogg_stream_packetin(&to, &op)) {
            OPRINT("ogg_stream_packetin() failed\n");
            exit(EXIT_FAILURE);
        }
    }

    /*
     * flush the rest of our headers. this ensures the actual data in each
     * stream will start on a new page, as per spec.
     */
    for(; ;) {
        int ret;

        ret = ogg_stream_flush(&to, &og);
        if(0 > ret) {
            OPRINT("ogg_stream_flush() failed\n");
            exit(EXIT_FAILURE);
        }
        if(0 == ret)
            break;
        fwrite(og.header, sizeof(uint8_t), og.header_len, tc->out);
        fwrite(og.body, sizeof(uint8_t), og.body_len, tc->out);
    }

    while(!tc->pglobal->stop) {
        if(encoder_get_raw_picture(pic)) {
            OPRINT("encoder_get_raw_picture() failed\n");
            exit(EXIT_FAILURE);
        }

        DBG("push picture to encoder\n");
        if(encoder_compress_picture(pic, tc->pglobal->stop)) {
            OPRINT("encoder_compress_picture() failed\n");
            exit(EXIT_FAILURE);
        }

        if(tc->pglobal->stop) {
            continue;
        }

        /* update buffer pool status */
        pthread_mutex_lock(&tc->bp.m);
        curr_raw = tc->bp.buffer_ptr[0];
        for(i = 0; i < tc->bp.num_buffers - 1; i++) {
            tc->bp.buffer_ptr[i] = tc->bp.buffer_ptr[i + 1];
        }
        tc->bp.buffer_ptr[i] = curr_raw;
        tc->bp.status >>= 1;
        pthread_cond_signal(&tc->bp.update);
        pthread_mutex_unlock(&tc->bp.m);
    }

    /* cleanup now */
    pthread_cleanup_pop(1);

    return NULL;
}
