/*******************************************************************************
#                                                                              #
#      MJPG-streamer allows to stream JPG frames from an input-plugin          #
#      to several output plugins                                               #
#                                                                              #
#      Copyright (C) 2007 Tom Stöveken                                         #
#                                                                              #
# This program is free software; you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation; version 2 of the License.                      #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program; if not, write to the Free Software                  #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    #
#                                                                              #
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

#include "../../mjpg_streamer.h"
#include "x264.h"
#include "x264_encoder.h"

static h264_context *hc = NULL;
static x264_t *h = NULL;
static x264_param_t param;

/******************************************************************************
Description.: initialize the H.264 encoder
Input Value.:
Return Value:
******************************************************************************/
static int encoder_init(void)
{
    DBG("encoder initialization\n");
    x264_param_default(&param);

    if(x264_param_default_preset(&param, "ultrafast", "zerolatency")) {
        OPRINT("x264_param_default_preset() failed\n");
        return -1;
    }

    param.i_width = hc->width;
    param.i_height = hc->height;
    param.i_frame_total = 0;
    param.rc.i_rc_method = X264_RC_ABR;
    param.rc.i_bitrate = hc->bitrate;
    param.i_fps_num = (int)(hc->fps * 1000 + 0.5);
    param.i_fps_den = 1000;

    if(x264_param_apply_profile(&param, "baseline")) {
        OPRINT("x264_param_apply_profile() failed\n");
        return -1;
    }

    if(NULL == (h = x264_encoder_open(&param))) {
        OPRINT("x264_encoder_open() failed\n");
        return -1;
    }
    x264_encoder_parameters(h, &param);

    OPRINT("x264 picture size %dx%d\n", param.i_width, param.i_height);
    return 0;
}

/******************************************************************************
Description.: terminate the H.264 encoder
Input Value.:
Return Value:
******************************************************************************/
static int encoder_term(void)
{
    if(h) {
        x264_encoder_close(h);
        DBG("encoder closed\n");
    }

    return 0;
}

/******************************************************************************
Description.: clean up allocated resources by the encoder thread
Input Value.: unused argument
Return Value:
******************************************************************************/
void encoder_cleanup(void *arg)
{
    static int first_run = 1;

    if(!first_run) {
        DBG("already cleaned up resources\n");
        return;
    }

    first_run = 0;
    OPRINT("cleaning up resources allocated by encoder thread\n");

    /*
     * conservative signaling in case the buffer pool is full and the grabber
     * is waititng on the conditional variable
     */
    pthread_mutex_lock(&hc->bp.m);
    hc->bp.status = 0;
    pthread_cond_signal(&hc->bp.update);
    pthread_mutex_unlock(&hc->bp.m);

    encoder_term();

}

/******************************************************************************
Description.: retrieve a raw picture from the buffer pool
Input Value.:
Return Value:
******************************************************************************/
static int encoder_get_raw_picture(x264_picture_t *pic)
{
    /* check if we have filled buffer in the buffer pool */
    pthread_mutex_lock(&hc->bp.m);

    /* keep waiting until at least one buffer is filled */
    while(!hc->pglobal->stop && 0 == hc->bp.status) {
        pthread_cond_wait(&hc->bp.update, &hc->bp.m);
    }

    pic->img.plane[0] = hc->bp.buffer_ptr[0];
    pthread_mutex_unlock(&hc->bp.m);

    if(hc->pglobal->stop) {
        return 0;
    }

    pic->i_pts = hc->num_frames++;
    pic->img.i_csp = X264_CSP_NV12;
    pic->img.i_plane = 2;
    pic->img.i_stride[0] =
    pic->img.i_stride[1] = hc->width;
    pic->img.plane[1] = pic->img.plane[0] + hc->width * hc->height;

    return 0;
}

/******************************************************************************
Description.: encode one frame and do proper house keeping
Input Value.:
Return Value:
******************************************************************************/
static int encoder_compress_picture(x264_picture_t *pic_in, x264_picture_t *pic_out)
{
    int frame_size;
    x264_nal_t *nal;
    int i_nal;

    if(0 > (frame_size = x264_encoder_encode(h, &nal, &i_nal, pic_in, pic_out))) {
        OPRINT("x264_encoder_encode() failed\n");
        return -1;
    }

    if(frame_size) {
        fwrite(nal[0].p_payload, sizeof(uint8_t), frame_size, hc->out);
    }

    hc->stream_size += frame_size;

    return 0;
}

/******************************************************************************
Description.: this is the main encoder thread
              it loops forever, takes a fresh frame from the buffer pool and
              encodes it
Input Value.: the exchange context
Return Value:
******************************************************************************/
void *encoder_thread(void *arg)
{
    x264_picture_t pic_in;
    x264_picture_t pic_out;
    uint8_t *curr_raw;
    int i;

    DBG("encoder thread running\n");

    hc = (h264_context *)arg;

    if(NULL == hc->out) {
        OPRINT("output file not opened\n");
        exit(EXIT_FAILURE);
    }

    /* set cleanup handler to cleanup allocated ressources */
    pthread_cleanup_push(encoder_cleanup, NULL);

    if(encoder_init()) {
        OPRINT("encoder_init() failed\n");
        exit(EXIT_FAILURE);
    }

    x264_picture_init(&pic_in);

    while(!hc->pglobal->stop) {
        if(encoder_get_raw_picture(&pic_in)) {
            OPRINT("encoder_get_raw_picture() failed\n");
            exit(EXIT_FAILURE);
        }

        if(hc->pglobal->stop) {
            continue;
        }

        DBG("push picture to encoder\n");
        if(encoder_compress_picture(&pic_in, &pic_out)) {
            OPRINT("encoder_compress_picture() failed\n");
            exit(EXIT_FAILURE);
        }

        /* update buffer pool status */
        pthread_mutex_lock(&hc->bp.m);
        curr_raw = hc->bp.buffer_ptr[0];
        for(i = 0; i < hc->bp.num_buffers - 1; i++) {
            hc->bp.buffer_ptr[i] = hc->bp.buffer_ptr[i + 1];
        }
        hc->bp.buffer_ptr[i] = curr_raw;
        hc->bp.status >>= 1;
        pthread_cond_signal(&hc->bp.update);
        pthread_mutex_unlock(&hc->bp.m);
    }
    while(x264_encoder_delayed_frames(h)) {
        if(encoder_compress_picture(NULL, &pic_out)) {
            OPRINT("encoder_compress_picture() failed\n");
            exit(EXIT_FAILURE);
        }
    }

    /* cleanup now */
    pthread_cleanup_pop(1);

    return NULL;
}
